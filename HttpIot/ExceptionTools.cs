﻿/*
 * HttpIot 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using System;
using System.Threading.Tasks;

namespace HttpIot
{
    /// <summary>   An exception tools. </summary>
    public class ExceptionTools
    {
        /// <summary>   Exception trap. </summary>
        ///
        /// <param name="task"> The task. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public static async Task ExceptionTrap(Func<Task> task)
        {
            try { await task().ConfigureAwait(false); } catch { }
        }
    }
}