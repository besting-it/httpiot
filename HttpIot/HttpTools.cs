﻿/*
 * HttpIot 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace HttpIot
{
    /// <summary>   A HTTP tools. </summary>
    public static class HttpTools
    {

        /// <summary>   Import certificate. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="file">     The file. </param>
        /// <param name="password"> The password. </param>
        ///
        /// <returns>   An X509Certificate2. </returns>
        public static X509Certificate2 ImportCertificate(string file, string password)
        {
            var cert = new X509Certificate2(File.ReadAllBytes(file), password, 
                X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
            if (!cert.HasPrivateKey)
                throw new InvalidOperationException("Certificate has no private key.");
            return cert;
        }
        
    }
}