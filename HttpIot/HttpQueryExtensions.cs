﻿/*
 * HttpIot 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace HttpIot
{
    /// <summary>   HTTP query extensions. </summary>
    public static class HttpQueryExtensions
    {
        /// <summary>   Get path arguments from Url. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   Dictionary containing path arguments. </returns>
        public static Dictionary<string, string> GetPathArguments(this HttpRequest request)
            => request.PathParams.ToDictionary(e => e.Key, e => HttpUtility.UrlDecode(e.Value));  
        
        /// <summary>   Get query arguments from Url. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   Dictionary containing query arguments. </returns>
        public static Dictionary<string, string> GetQueryArguments(this HttpRequest request)
        {
            var arguments = new Dictionary<string, string>();
            string urlQuery = request.Url.Contains("?") ? 
                request.Url.Substring(request.Url.IndexOf("?", StringComparison.InvariantCulture) + 1) : string.Empty;
            if (urlQuery.Length == 0)
                return arguments;
            var qs = HttpUtility.ParseQueryString(urlQuery, request.Charset);
            foreach (string key in qs.AllKeys)
                arguments.Add(key, HttpUtility.UrlDecode(qs[key]));
            return arguments;
        }

        /// <summary>
        /// Gets form arguments from body. Note that body will be read in full using this method. Reading
        /// from stream afterwards is not possible.
        /// </summary>
        /// 
        /// <param name="request">  The request. </param>
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        /// 
        /// <returns>   Dictionary containing form arguments. </returns>
        public static async Task<Dictionary<string, string>> GetFormArguments(this HttpRequest request, CancellationToken token)
        {
            var arguments = new Dictionary<string, string>();
            if (!request.Headers.ContainsKey(HttpConstants.KeyContentType))
                return arguments;
            var contentType = request.Headers[HttpConstants.KeyContentType];
            if (contentType.ToLowerInvariant().Contains(HttpConstants.ValueContentTypeForm))
            {
                var qs = HttpUtility.ParseQueryString(await request.GetBody(token).
                    ConfigureAwait(false), request.Charset);
                foreach (string key in qs.AllKeys)
                    arguments.Add(key, qs[key]);
            }
            return arguments;
        }
    }
}