# README #

HttpIoT is a truly portable, zero-dependency, lightweight .NET standard Http server. It is easy-to-use and supports convenient routing.

The reason for creating HttpIoT is that the legacy .NET [HttListener](https://docs.microsoft.com/en-us/dotnet/api/system.net.httplistener?view=net-5.0) lacks SSL support for Linux and needs a namespace reservation on Windows. Other solutions require ASP.NET.

Licensed under the Apache License, Version 2.0.

## Features ##

* Very simple to use 
* Small footprint: a single 70kb dll
* Based on .NET TcpListener and SslStream
* Template-based routing (using brackets in path definitions)
* Uses async/await internally and requires dependent code to use it as well
* Supports Ipv4 and Ipv6
* Supports keep-alive and chunked requests / responses
* Supports SSL (TLS 1.0 - 1.3)
* Supports simple form, query and template argument processing
* No session / cookie / file upload / multipart form support (yet)

## Nuget ##

[https://www.nuget.org/packages/HttpIot](https://www.nuget.org/packages/HttpIot)

## Coding guidelines ##

This library makes heavy use of the [Task-Based Asynchronous Pattern (TAP)](https://docs.microsoft.com/en-us/dotnet/standard/asynchronous-programming-patterns/task-based-asynchronous-pattern-tap), which means dependent code must use it as well.
If you're not familiar with async code, please read these [best practices](https://docs.microsoft.com/en-us/archive/msdn-magazine/2013/march/async-await-best-practices-in-asynchronous-programming). Pay special attention to the rule __never block in async code__.
That means, always use __await__ in async methods and encapsulate blocking code with __await Task.Run(() => { ... })__. 

Additionally, prefer __ConfigureAwait(false)__ if you don't need to return to the UI context (not used in the snippets here for simplicity).

## Examples ##

```csharp
var server = new HttpServer();
server.Failing += (sender, args) => Console.WriteLine(args.Exception.Message);

var config = new HttpServerConfig()
{
    Port = 8080
};

server.AddRoute("/api/v1/helloworld", async (rq, rp, token) =>
{
    await rp.AsText("Hello world", token);
}, HttpMethod.Get);  

server.Start(config);  // Does not block
```

### SSL ###

Example configuration for SSL connection using PKCS12 password encrypted store (private key, X.509 certificate).

```csharp
var options = new SslAuthenticationOptions() 
{
    Protocols = SslProtocols.Tls12,  
}

options.Certificates.Add(HttpTools.ImportCertificate("keys.p12", "password"));

var config = new HttpServerConfig()
{
    Port = 8080,
    UseHttps = true,
    SslAuthenticationOptions = options
};
```

#### Prerequisites for TLS 1.3 ####

For TLS13 use ```SslProtocols.Tls13``` or, if your target framework is missing this enum value use ```Protocols = SslProtocolsExtensions.Tls13```.

* On a Windows system, you might have to create some additional [registry entries](https://docs.microsoft.com/de-de/windows-server/security/tls/tls-registry-settings#tls-12). The name of the key must be __TLS 1.3__.
* On a Linux system OpenSSL version 1.1.1 (or later) must be installed.

## Routing and arguments processing ##

Examples for processing request arguments. Methods can be combined.

### Template arguments in Url path. ###

Simply put path variables in brackets, for example cosider this Url: http://127.0.0.1:8080/api/v1/helloworld/123

```csharp
server.AddRoute("/api/v1/helloworld/{key}", async (rq, rp, token) =>
{
    var pathArgs = rq.GetPathArguments()
    var val = pathArgs["key"];
    await rp.AsText($"Hello world, value is {val}", token);
}, HttpMethod.Get);  
```

Output: Hello world, value is 123

### Query and form arguments ###

Use the following function for Urls containing query arguments like http://127.0.0.1:8080/api/v1/helloworld?key=123

```csharp
var args = rq.GetQueryArguments();
var val = args["key"];  // You might want to check if the key really exists
```

Use the following function for parsing form arguments from Http body (HttpMethod.Post)

```csharp
var formArgs = rq.GetFormArguments(); 
```

## Getting the HTTP body ##

Http request entities can be processed as simple string by using the __GetBody__ method.

```csharp
server.AddRoute("/api/v1/helloworld", async (rq, rp, token) =>
{
    if (rq.HasEntityBody) 
    {
        var body = await rq.GetBody(token);
        
        // Process request here...

        // Response
        rp.StatusCode = 204;  // Ok, no content
        rp.ContentType = null;
    }
    else 
    {
        rp.StatusCode = 400;  // Bad request
        rp.ContentType = null;
    }
}, HttpMethod.Post);  
```

### Reading streams ###

Processing request and response as stream is possible by using __rq.AsStream()__ and __rp.AsStream()__, resp.
Using these methods you can read and write directly from and to streams.

The implementation of the __GetBody__ method shows how to read streams (code just copied in the request handler below).

```csharp
server.AddRoute("/api/v1/helloworld", async (rq, rp, token) =>
{
    if (rq.HasEntityBody) 
    {
        // This is what actually happens when you call 'GetBody':         
		var bytes = new byte[8192];
		var sb = new StringBuilder();
		int len;
        
        // Start reading the request input stream
		while ((len = await request.AsStream().ReadAsync(bytes, 0, bytes.Length, token)) > 0)
		{
			var data = len == bytes.Length ? bytes : new byte[len];
			if (len != bytes.Length)
				Array.Copy(bytes, 0, data, 0, len);
			sb.Append(request.Charset.GetString(data));
		}
		var body = sb.ToString();  // Here it is!
        
        // Process request here...

        // Response
        rp.StatusCode = 204;  // Ok, no content
        rp.ContentType = null;
    }
    else 
    {
        rp.StatusCode = 400;  // Bad request
        rp.ContentType = null;
    }
}, HttpMethod.Post);  
```

## Serving files: whitecard routes, writing streams ##

The following example shows how to configure a route for using the suffix Url part as file path for serving files.
You can simply append a __*__ to any route definition to forward all requests which have Urls starting with the prefix.

```csharp
// A simple file serving route. Example request: http://192.168.1.1/content/Folder/Text.txt
var staticPrefix = "/content/";

// Add whitecard route using "*" at the end, i.e. /content/*
server.AddRoute($"{staticPrefix}*", async (rq, rp, token) =>
{
    // Get the file path suffix
    var filePath = rq.Url.Substring(staticPrefix.Length);  // "Folder/Text.txt"
    
    if (!File.Exists(filePath))
    {
        rp.StatusCode = 404;
        rp.ContentType = string.Empty;
        return;
    }
    
    // You might want to change the content type depending on which files are requested (see mime link below)
    rp.ContentType = "application/octet-stream";
    
    // Open the file (note: all files are relative to the working directory in this case)
    var stream = File.OpenRead(filePath);
    
    // Copy file stream to response stream 
    await stream.CopyToAsync(rp.AsStream(), 8192, token);
}, HttpMethod.Get);   
```

For mapping mime types to the content type, see for example the [GetMimeMapping](https://docs.microsoft.com/en-us/dotnet/api/system.web.mimemapping.getmimemapping) method.
